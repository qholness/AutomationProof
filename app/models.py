from app import database
from sqlalchemy import (
    Column, Integer, String, Boolean
    ,ForeignKey, Float, Text, DateTime
)
from werkzeug.security import check_password_hash, generate_password_hash


class Visitor(database.Model):
    __tablename__ = "visitors"
    id = Column(Integer, primary_key=True)
    ip = Column(String(140), unique=True, nullable=False)
    created_timestamp = Column(DateTime(6))
    last_visit = Column(DateTime(6))
    profid = Column(Integer) # FK to professions.id


    def find_visitor(self, ip):
        for i in self.query.all():
            if check_password_hash(i.ip, ip):
                return i
        return None

class Demographics(database.Model):
    __tablename__ = "demographics"
    id = Column(Integer, primary_key=True)
    ip = Column(ForeignKey("visitors.ip"))
    userid = Column(String(140))
    age = Column(Integer)
    gender = Column(Integer)
    profid = Column(Integer) # FK to professions.id
    wage = Column(Float)
    preferredWage = Column(Float)
    city = Column(String(140))
    state = Column(String(2))
    country = Column(String(100))
    location = Column(String(140))
    created_timestamp = Column(DateTime(6))
    updated_timestamp = Column(DateTime(6))
    

class Users(database.Model):
    __tablename__ = "users"
    id = Column(Integer, primary_key=True)
    userid = Column(String(140))
    username = Column(String(140), unique=True, nullable=False)
    email = Column(String(140))
    password = Column(String(140), unique=True, nullable=False)
    isAdmin = Column(Boolean)

    def verify_password(self, password):
        return check_password_hash(self.password, password)

    def hash_password(self, password):
        self.password = generate_password_hash(password)
        return self.password


class ProfessionCategories(database.Model):
    __tablename__ = "professionscategories"
    id = Column(Integer, primary_key=True)
    categoryname = Column(String(140), unique=True, nullable=False)
    description = Column(String(280), unique=True, nullable=False)


class Profession(database.Model):
    __tablename__ = "professions"
    id = Column(Integer, primary_key=True)
    title = Column(String(140), unique=True, nullable=False)
    description = Column(String(280), unique=True, nullable=False)
    categoryid = Column(ForeignKey('professionscategories.id'))
    bls_url = Column(String(280))
    img_url = Column(String(280))


class Responses(database.Model):
    __tablename__ = "responses"
    id = Column(Integer, primary_key=True)
    profid = Column(ForeignKey("professions.id"))
    userid = Column(String(140))
    valuation = Column(Float)
    automationPreference = Column(Float)
    freeformResponse = Column(Text(500))
    created_timestamp = Column(DateTime(6))
    updated_timestamp = Column(DateTime(6))


    def __repr__(self):
        return "user: {} | profession: {}".format(self.userid, self.profid)

