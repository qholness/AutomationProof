from app import dpj
from app.forms import LoginForm, RegistrationForm
from app.Utilities.auth import (
    login_user, logout_user, register_user
)
from flask import request, redirect, url_for, render_template



@dpj.context_processor
def auth_injection():
    return dict(
        loginForm = LoginForm(request.form)
    )


@dpj.route("/register")
def RegisterPage():
    form = RegistrationForm(request.form)
    return render_template("auth/register.html", form=form)


@dpj.route("/register-submit", methods=["POST"])
def Register():
    form = RegistrationForm(request.form)
    response = register_user(form)
    return redirect(url_for("index"))

        

@dpj.route("/login", methods=["POST"])
def login():
    form = LoginForm(request.form)
    login = login_user(form)
    if login:
        return redirect(url_for("index"))
    return redirect(url_for("index"))


@dpj.route("/logout")
def logout():
    logout_user()
    return redirect(url_for("index"))
