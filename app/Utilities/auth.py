from app import database, dpj
from app.models import Users
from app.Utilities.nav import redirect_url
from flask import flash, session, redirect, url_for
from werkzeug.security import generate_password_hash
from sqlalchemy.exc import IntegrityError

def register_user(form):
    u = Users.query.filter_by(username=form.username.data).first()

    if not u:
        u = Users()
        u.userid = form.username.data
        u.username = form.username.data
        u.password = u.hash_password(form.password.data)
        u.email = form.email.data
        
        try:
            database.session.add(u)
            database.session.commit()
            flash("You have successfuly registered.")
            return redirect(url_for("index"))

        except IntegrityError as ie:
            dpj.logger.info("Registration failed")
            flash("Registration failed. A user is using that email.")
    
    flash("Username is already taken.", category="warning")
    return redirect(redirect_url())


def login_user(form):
    u = Users.query.filter_by(username=form.username.data).first()

    if not u:
        flash("No record found for that username.", category="danger")
        return False

    if u.verify_password(form.password.data):
        # Login user
        session['logged_in'] = True
        session['username'] = u.username
        session['email'] = u.email
        session['userid'] = u.userid # Overrides IP user id in session
        session['isAdmin'] = u.isAdmin

        return redirect(redirect_url())

    flash("Invalid password")
    return redirect(redirect_url())


def logout_user():
    session.clear()
    return True
