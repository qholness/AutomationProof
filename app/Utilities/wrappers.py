from functools import wraps
from flask import session, flash, redirect, url_for




def logged_in(func, *args, **kwargs):
    @wraps(func)
    def decorated(*ags, **kwargs):
        if session.get('logged_in'):
            return func(*args, **kwargs)
        flash("You must be logged in to view this page", category="warning")
        return redirect(url_for('index'))
    return decorated


def not_logged_in(func, *args, **kwargs):
    @wraps(func)
    def decorated(*ags, **kwargs):
        if not session.get('logged_in'):
            return func(*args, **kwargs)
        return redirect(url_for('index'))
    return decorated
