from app.models import Responses, Demographics
from app.Utilities.timestamp import timestamp
from app.Utilities.gets import getVisitor
from flask import session, request




def genNewProfessionResponse(profid):
    response = Responses()
    response.created_timestamp = timestamp()
    response.userid = session['userid']
    response.profid = profid
    return response


def generateNewDemographic(Form):
    """Generate new demographic data"""
    newDemo = Demographics()
    visitor = getVisitor()
    newDemo.ip = visitor.ip
    newDemo.userid = session['userid']
    newDemo.age = Form.age.data
    newDemo.gender = Form.gender.data
    newDemo.profid = int(request.form['profid'])
    newDemo.city = Form.city
    newDemo.state = Form.state
    newDemo.wage = Form.wage.data
    newDemo.location = Form.freeformLocation.data
    newDemo.updated_timestamp = timestamp()
    newDemo.created_timestamp = timestamp()
    return newDemo