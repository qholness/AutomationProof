from flask import session
from app.models import (
    Profession, Responses
)

def CountAvailableRatings():
    if not session.get('userid'):
        return 0
    professions = set(i.id for i in Profession.query.all())
    ratedProfessions = set(i.profid for i in Responses
        .query
        .filter_by(userid=session['userid'])
        .all())
    return len(list(professions - ratedProfessions))