from app import database
from app.models import (
    Visitor, Demographics, Profession, Responses
)
from werkzeug.security import generate_password_hash
from flask import session, request
from app.Utilities.timestamp import timestamp



def getVisitor():
    visitor = Visitor()\
        .find_visitor(request.remote_addr)
    return visitor

def getDemographic():
    demo = Demographics.query.filter_by(userid=session['userid']).first()
    return demo

def generateVisitor():
    visitor = Visitor()
    visitor.ip = generate_password_hash(request.remote_addr) # Hash visitor IP to avoid security issues
    visitor.created_timestamp = timestamp()
    return visitor

def getRateableProfessions():
    """Generate list of professions user can rate"""
    professions = set(i.id for i in Profession.query.all())
    ratedProfessions = set(i.profid for i in Responses.query.filter_by(userid=session['userid']).all())
    # Set operation to examine difference.
    rateables = list(professions - ratedProfessions)    
    return rateables