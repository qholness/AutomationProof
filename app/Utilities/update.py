from app.Utilities.timestamp import timestamp
from flask import session, request



def updateProfessionResponse(Resp, Form, profid):
    """edit the existing response object with new data"""
    Resp.updated_timestamp = timestamp()
    Resp.valuation = Form.valuation.data
    Resp.automationPreference = Form.automationPreference.data
    Resp.freeformResponse = Form.freeformResponse.data
    return Resp


def updateDemographicData(Demo, Form):
    profid = int(request.form['profid'])
    Demo.updated_timestamp = timestamp()

    if profid > 0:
        Demo.profid = profid # is not in form class

    if Form.age.data:
        Demo.age = Form.age.data

    if Form.gender.data:
        Demo.gender = Form.gender.data

    if Form.city and Form.state:
        Demo.city = Form.city
        Demo.state = Form.state

    if Form.freeformLocation.data and not Form.city:
        Demo.location = Form.freeformLocation.data
        Demo.city = None
        Demo.state = None

    if Form.wage.data:
        Demo.wage = Form.wage.data
    
    if Form.preferredWage.data:
        Demo.preferredWage = Form.preferredWage.data

    return Demo


def popProfession(profid):
    if session.get('unratedProfessions'):
        if int(profid) in session.get('unratedProfessions'):
            idx = session['unratedProfessions'].index(int(profid))
            session['unratedProfessions'].pop(idx)
    return session