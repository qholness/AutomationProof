import random
from app import dpj
from flask import render_template
from bokeh.plotting import figure
from bokeh.resources import CDN
from bokeh.embed import file_html, components





@dpj.route("/charts")
def chart():
    plot = figure()
    plot.circle([1,2], [3,4])

    script, div = components(plot)

    return render_template("bokeh/chart.html"
        ,the_div=div, the_script=script)