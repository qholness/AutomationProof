from random import choice as random_choice
from datetime import datetime
from functools import wraps
from app import (
    dpj, database, article_count
)
from app.models import (
    Visitor, Responses, Profession
    ,Demographics
)
from app.forms import (
    ValuationForm, DemographicsForm,
    LoginForm
)
from flask import (
    request, render_template, session, redirect
    , flash, get_flashed_messages, url_for, g
)
from werkzeug.security import (
    check_password_hash, generate_password_hash
)
from app.Utilities.gets import (
    getDemographic, getVisitor, generateVisitor
    ,getRateableProfessions
)
from app.Utilities.stats import (
    CountAvailableRatings
)
from app.Utilities.generate import (
    genNewProfessionResponse, generateNewDemographic
)
from app.Utilities.update import (
    updateProfessionResponse, updateDemographicData
    ,popProfession
)
from app.Utilities.wrappers import (
    logged_in, not_logged_in
)
from app.Utilities.timestamp import timestamp
from app.Utilities.nav import redirect_url

@dpj.context_processor
def inject_context():
    """Getting active count of database changes"""
    return dict (
        availableRatings = lambda : CountAvailableRatings(),
        ratingCount = lambda : len(Responses.query.all()),
        demographicsGathered = lambda : len(Demographics.query.all()),
        visitorCount = lambda : len(Visitor.query.all()),
        professionCount = lambda : len(Profession.query.all()),
        artCount = lambda : article_count(),
        sorted_professions = lambda: sorted(Profession.query.all(), key=lambda x: x.title),
        Profession = Profession,
        getattribute = getattr,
        absvalue = abs
    )


@not_logged_in
@dpj.before_request
def captureIP():
    """Capture IP address and create a user entry. This could get bad :P"""
    visitor = getVisitor()
    
    if not visitor:
        # Visitor not found. Create a new one
        visitor = generateVisitor()
        database.session.add(visitor)
    
    visitor.last_visit = timestamp() # Update visit time

    if not session.get("userid"):
        session['userid'] = visitor.id
    if not session.get("professions"):
        session['professions'] = [i.id for i in Profession.query.all()]
    
    database.session.commit()
    return


@dpj.route("/")
def index():
    """Splash page for users"""
    return render_template("index.html")


@dpj.route("/rate-someone")
def Rate(has_responded=0):
    """Navigate user to a psuedo-randomly selected profession to rate"""
    
    if session.get('unratedProfessions'):
        # User has available professions to rate in their session
        profid = random_choice(session['unratedProfessions'])
        return redirect(url_for("RateProfession", profid=profid, responded=has_responded))
    
    # Otherwise, create available ratings
    session['unratedProfessions'] = getRateableProfessions()

    if not session['unratedProfessions']:
        if not session.get('reminded'):
            # Remind users to checkout statistics pages
            # flash("Checkout the statitics pages.", category="success")
            session['reminded'] = 1
        # User has finish all possible ratings. Redirect to a random one.
        if session['professions']:
            profid = random_choice(session['professions'])
            flash("You have already rated this occupation. Submitting this form again will update your rating", category="warning")
            has_responded = 1
            return redirect(url_for("RateProfession", profid=profid, responded=has_responded))
        else:
            flash("No available choices", category="info")
            return redirect(url_for("index"))
    
    return redirect(url_for('Rate', has_responded=has_responded))


@dpj.route("/profession/<profid>/<responded>")
def RateProfession(profid, responded=0):
    """Render rating form"""
    form = ValuationForm(request.form)
    profession = Profession.query.filter_by(id=profid).first()
    if profession:
        return render_template("ratings/rate.html", form=form, profession=profession, responded=responded)
    flash("No available data at the moment", category="info")
    return redirect(redirect_url())


@dpj.route("/submit/<profid>/<responded>", methods=["POST"])
def SubmitValuation(profid, responded=0):
    """Submit rating and redirect to another occupation to rate"""
    form = ValuationForm(request.form)
    response = Responses.query.filter_by(userid=session['userid'], profid=profid).first()

    if not response:
        # Create a new response
        response = genNewProfessionResponse(profid)

    response = updateProfessionResponse(response, form, profid)
    database.session.add(response)
    database.session.commit()
    popProfession(profid)
    flash("Cool! Thanks for rating. Feel free to rate another.", category="success")
    return redirect(url_for("Rate"))


@dpj.route("/my-demographics", methods=["GET"])
def MyDemographics():
    """Allow users to enter their demographic information."""
    form = DemographicsForm(request.form)
    demo = getDemographic()
    
    if demo:
        flash("You have already submitted a demographic profile but are free to update your details by submitting this form again", category="info")

    return render_template("demographics/demographics.html", form=form
        , demo=demo)


@dpj.route("/submit-demographics", methods=["POST"])
def SubmitDemographics():
    form = DemographicsForm(request.form).prep()
    demo = getDemographic()

    if demo:
        demo = updateDemographicData(demo, form)
        flash("Demographic data updated", category="success")
    else:
        database.session.add(generateNewDemographic(form))
        flash("Demographic data inserted", category="success")

    database.session.commit()
    return redirect(redirect_url())


@dpj.route("/about")
def About():
    """About page"""
    return render_template("about.html", title="About")

@dpj.route("/select-occupation")
def SelectOccupationToRate():
    return render_template("selectocc.html", title="Pick to Rate")


@dpj.route("/summary/<profid>")
def SummaryStatistics(profid):
    return "Summary Statistics yo"