function getData(url, targetdiv, form){
    var XHR = new XMLHttpRequest();

    // Bind the FormData object and the form element
    var FD = new FormData(form);

    // Define what happens on successful data submission
    XHR.addEventListener("load", function(event) {
        var response = event.target.responseText;
        var target = document.getElementById(targetdiv);
        target.innerHTML = response;
    });

    // Define what happens in case of error
    XHR.addEventListener("error", function(event) {
        alert('Oups! Something goes wrong.');
    });

    // Set up our request
    XHR.open("GET", url);

    // The data sent is what the user provided in the form
    XHR.send(FD);

}

function getPandasJSON(url, targetdiv, form){
    var XHR = new XMLHttpRequest();

    // Bind the FormData object and the form element
    var FD = new FormData(form);

    // Define what happens on successful data submission
    XHR.addEventListener("load", function(event) {
        var response = JSON.parse(event.target.responseText);
        var target = document.getElementById(targetdiv);
        target.innerHTML = JSON.stringify(response);
    });

    // Define what happens in case of error
    XHR.addEventListener("error", function(event) {
        alert('Oups! Something goes wrong.');
    });

    // Set up our request
    XHR.open("GET", url);

    // The data sent is what the user provided in the form
    XHR.send(FD);

}