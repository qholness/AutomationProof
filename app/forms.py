from wtforms import Form
from wtforms.fields import (
    DecimalField, TextAreaField, IntegerField
    , SelectField, StringField, PasswordField
)
from wtforms.fields.html5 import DecimalRangeField
from wtforms import validators
import us
import iso3166
import pycity

def generate_city_choices():
    state_codes = [u"US-%s" % i.abbr for i in us.STATES]
    major_cities = [pycity.cities.get(code=code) for code in state_codes]
    city_choices = []
    for m in major_cities:
        for c in m:
            state = c['code'][-2:]
            city_choices.append(", ".join([c['name'], state]))
    return city_choices


class LoginForm(Form):
    username = StringField(validators=[validators.InputRequired()])
    password = PasswordField(validators=[validators.InputRequired()])

class RegistrationForm(Form):
    firstname = StringField(label="First Name")
    lastname = StringField(label="Last Name")
    username = StringField(label="Username")
    email = StringField('Email Address',
        validators=[validators.InputRequired(), validators.Email(), validators.Regexp('[^@]+@[^@]+\.[^@]+')]) # Use for email verification
    password = PasswordField('New Password', [
        validators.InputRequired(), validators.length(min=8, max=20),
        validators.EqualTo('confirm', message='Passwords must match')
    ])
    confirm = PasswordField(validators=[validators.DataRequired()])


class ValuationForm(Form):
    automationPreference = DecimalRangeField("Preference for automation")
    valuation = DecimalField("Per hour valuation (wage)", places=2)
    freeformResponse = TextAreaField("Other details"
        ,validators=[validators.length(max=500)])
    min_wage = 7.25
    max_wage = 300.00 # Not accepting wages above this

    def validate(self):
        if 0 < self.valuation < self.max_wage == False:
            return False
        if self.valuation.data:
            self.valuation.data = max(
                [
                    max((self.min_wage, self.valuation.data))
                    ,min((self.max_wage, self.valuation.data))
                ]
            )
        return True

class DemographicsForm(Form):
    age = SelectField(choices=[(0,0)] + [(i,i) for i in range(18, 121)], coerce=int)
    gender = SelectField(choices=[(0, "Male"), (1, "Female"), (2, "Other"), (3,'')], coerce=int)
    location = SelectField(choices=[(i, i) for i in generate_city_choices()] + [(None, "City, State")])
    freeformLocation = StringField(validators=[validators.length(max=140)])
    wage = DecimalField("Your wage", places=2)
    preferredWage = DecimalField("Your \"Preferred\" wage", places=2)
    min_wage = 7.25
    max_wage = 300.00 # Not accepting wages above this
    city = None
    state = None
    max_age = 120

    def prep(self):
        self.age.data = self.age.data if 0 < self.age.data < self.max_age else None
        self.gender.data = None if self.gender.data == 3 else self.gender.data
        if self.wage.data:
            self.wage.data = self.max_wage if self.wage.data > self.max_wage else self.wage.data
        if self.preferredWage.data:
            self.preferredWage.data = self.max_wage if self.preferredWage.data > self.max_wage else self.preferredWage.data
        if self.location.data:
            if "," in self.location.data:
                self.city, self.state = self.location.data.split(",")
        if len(self.freeformLocation.data) == 0:
            self.freeformLocation.data = None
        
        return self