import os
from flask import Flask, g
from flask_mail import Mail
from flask_sqlalchemy import SQLAlchemy as Alfonse
from flask_migrate import Migrate
from sqlalchemy.exc import OperationalError
import uuid


dpj = Flask(__name__)
dpj.config.from_pyfile("config.cfg")
PROJECT_ROOT = os.path.dirname(os.path.realpath(__file__))
DATABASE = os.path.join(PROJECT_ROOT, 'data', 'data.db')
with open(DATABASE, "a+") as db: pass # Create database if it is not existent
dpj.config["SQLALCHEMY_DATABASE_URI"] = "sqlite:///{}".format(DATABASE)
dpj.config["SECRET_KEY"] = str(uuid.uuid4) # Prod key
database = Alfonse(dpj)
migrate = Migrate(dpj, database)



from app.models import Profession as PR
from app.models import ProfessionCategories as PC
from setup import article_count

from app import views, models, database, migrate
from app.auth import views
from app.bokeh import views