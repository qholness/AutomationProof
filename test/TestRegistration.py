import os
import unittest
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from time import sleep
import names
from random import shuffle


class TestRegistration(unittest.TestCase):
    def test_00_register(self):
        driver.get(base_url + "/register")
        for fname, lname, uname, pwd in users:
            firstname = driver.find_element_by_id("firstname")
            lastname = driver.find_element_by_id("lastname")
            username = driver.find_element_by_id("username")
            password = driver.find_element_by_id("password")
            confirm = driver.find_element_by_id("confirm")
            firstname.send_keys(fname)
            lastname.send_keys(lname)
            username.send_keys(uname)
            password.send_keys(pwd)
            confirm.send_keys(pwd)
            confirm.send_keys(Keys.RETURN)
            if driver.current_url != base_url + '/register':
                driver.get(base_url + "/register")


                
                    
    def test_01_login_users(self):
        driver.get(base_url)
        for fname, lname, uname, pwd in users:
            with open (user_file, "a") as usersfile:
                usersfile.write("({}+{}),".format(uname, pwd))
            username = driver.find_element_by_id("username")
            password = driver.find_element_by_id("password")
            username.send_keys(uname)
            password.send_keys(pwd)
            password.send_keys(Keys.RETURN)
            driver.get(base_url + "/logout")
        driver.close()


if __name__ == "__main__":
    global driver
    global base_url
    global user_count
    global user_file
    driver = webdriver.Chrome()
    base_url = "http://localhost:5000"
    users = set()
    user_count = 25
    user_file = "users.csv"

    while len(users) < user_count:
        users.add(names.get_full_name())
        
    def shuffled(u):
        u = [i for i in u]
        shuffle(u)
        return u


    users = [
        [
        u.split(" ")[0]
        ,u.split(" ")[1]
        ,u.replace(" ", "")
        ,"".join(shuffled(u))
        ]
        for u in users
    ]
    driver.maximize_window()

    with open (user_file, "w+"):
        pass
    unittest.main()