import os
import unittest
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.common.exceptions import TimeoutException
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
from time import sleep
from flask import session


class TestLogin(unittest.TestCase):
    def test_00_login(self):
        driver.get(base_url)

    def test_01_about(self):
        driver.get(base_url + "/about")
    
    def test_02_rate_someone(self):
        driver.get(base_url + "/rate-someone")
    
    def test_03_my_demographics(self):
        driver.get(base_url + "/my-demographics")
    
    def test_04_select_occupation(self):
        driver.get(base_url + "/select-occupation")
    
    def test_05_register(self):
        driver.get(base_url + "/register")


if __name__ == "__main__":
    global driver
    global url
    driver = webdriver.Chrome()
    driver.maximize_window()
    base_url = "http://localhost:5000"
    unittest.main()
