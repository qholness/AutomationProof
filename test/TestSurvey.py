import os
import unittest
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.common.exceptions import TimeoutException
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
from time import sleep
import random
from parseusers import parse_usernames



class TestSurvey(unittest.TestCase):
    """Test survey operations"""
    def test__00__test(self):
        with open(user_file, "r") as users:
            data = users.readline().split(",")
            for i in data[:-1]:
                i = parse_usernames(i)
                uname = i[0]
                pwd = i[1]
                driver.get(base_url)
                
                username = driver.find_element_by_id("username")
                password = driver.find_element_by_id("password")
                username.send_keys(uname)
                password.send_keys(pwd)
                password.send_keys(Keys.RETURN)
                
                driver.get(base_url + "/rate-someone")
                sleep(2)

                slider = driver.find_element_by_id("automationInput")
                valuation = driver.find_element_by_id("valuation")

                for i in range(random.randint(0, 5)):
                    slider.send_keys(Keys.RIGHT)
                
                valuation.send_keys(str(random.uniform(7.25, 300)))
                valuation.send_keys(Keys.RETURN)
                sleep(5)

                driver.get(base_url + "/logout")
                
        driver.close()




if __name__ == "__main__":
    global driver
    global base_url
    global user_file
    driver = webdriver.Chrome()
    driver.maximize_window()
    base_url = "http://localhost:5000"
    user_file = "users.csv"
    unittest.main()

