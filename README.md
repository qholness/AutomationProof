What jobs are really going away?

Article after article dictates X job will not be here 10 years from now. If you happen to be one of the targets, do you honestly feel that your job is going away?<br>
Does your network?<br>
This survey is meant to capture the valuation others place on your profession and allow you to place a particular value on others profession.<br>
<br>
For instance, how much do you value your lawyer and would you rather their services be replaced with a contextual electronic interface (read: a computer).<br>
The inspiration from this survey comes from being within the industry and frequently hearing from developers the opinion that automation will eliminate all jobs.<br>
Though I do believe in a future on optimizing health over career and freedom over trade, I don't do so without gathering statitistics on the matter.<br>
What's your opinion on the topic?<br>


<User Experience>
<Introduction page> --> Demographic data--> Survey --> Results so far--> Statistical additions source data and exploration features