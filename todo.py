class ToDo:
    """Class intended for managing to do list"""
    
    def ProjectSpecification(self):
        return """Project Spec"""

    def fixes(self):
        return """
            Visitors in models.py should have `created_timestamp` and `updated_timestamp` columns instead of just visitTime.
        """

    def about(self):
        return """
            Detail about the project, author and usage
        """

    def formValidation(self):
        return """
            Form validation standards and functions
        """

    def DataAnalysisIdeas(self):
        return """
        Summary statistics: 
            What proportion of jobs listed would the population preferred automated?
            Top-10 most preferred automated?
            Automation distribution examination?
            Wage vs. Automation Preference?
            Cross-examination - Compare user profession rating with ratings of others?
            Geographic concentration of preferences.
            
        Occupation Specific Statistics:
            Automation Preference Meter?
            Preferred Wage by entrants?
            Wage given by survey takers?

        Cross-examination:

        Meta-statistics:
            Visitors by IP
            Demographics MetaData:
                Male vs. Female desired wage comparison by profession
                Male vs. Female actual wage comparison by profession
                Age/Gender breakdowns
        """