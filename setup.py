import os
import csv
import asyncio
from app.models import (
    Users, Profession, ProfessionCategories
)
from app import database as db
from app import PROJECT_ROOT as ROOT



def article_count():
    """Read in automationarticles.csv and count lines"""
    root = os.path.dirname(os.path.realpath(__file__))
    articles = os.path.join(root, 'app', 'data', 'automationarticles.csv')
    count = 0
    with open(articles, 'r') as arts:
        reader = csv.reader(arts)
        for idx,row in enumerate(arts):
            if idx > 0:
                count += 1
    return count
    
def init_db():
    """Database initialization"""
    categoriescsv = os.path.join(ROOT, 'data', 'categories.csv')
    professionscsv = os.path.join(ROOT, 'data', 'occupations.csv')
    try:
        db.session.execute("DELETE FROM professionscategories;")
        db.session.execute("DELETE FROM professions;")
        db.session.commit()
    except:
        pass

    @asyncio.coroutine
    def inject_categories():
        """Insert categories from categories.csv into professionscategories table"""
        with open(categoriescsv, 'r') as cats:
            catreader = csv.reader(cats, delimiter=",")
            for idx, row in enumerate(catreader):
                if idx > 0:
                    p = ProfessionCategories()
                    p.id, p.categoryname, p.description = row
                    db.session.add(p)
        return


    @asyncio.coroutine
    def inject_professions():
        """Insert professions from professions.csv into professionscategories table"""
        with open(professionscsv, 'r') as profs:
            profreader = csv.reader(profs, delimiter=",")
            for idx, row in enumerate(profreader):
                if idx > 0:
                    p = Profession()
                    p.bls_url, p.categoryid, p.description, p.title = row
                    db.session.add(p)
        return

    @asyncio.coroutine
    def create_admin():
        u = Users.query.filter_by(username='admin').first()
        if u:
            return
        u = Users()
        u.userid = "adminmin"
        u.username = "admin"
        u.hash_password("tacos121")
        u.email = "qholness@gmail.com"
        u.isAdmin = True
        db.session.add(u)
        return


    def run_async(funcs=[]):
        """Run asynchronous functions"""
        loop = asyncio.new_event_loop() # Create a new loop
        asyncio.set_event_loop(loop) # Set event loop to new loop
        tasks = [asyncio.ensure_future(f()) for f in funcs] # Get list of functions to run passed to async.ensure_future
        loop.run_until_complete(asyncio.wait(tasks)) # Run insertions
        loop.close()
        return

    run_async(funcs=[inject_categories, inject_professions, create_admin])
    db.session.commit()


if __name__ == "__main__":
    init_db()