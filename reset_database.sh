# Before to reset the database before deployment
rm app/data/data.db migrations/ -rf
touch app/data/data.db
export FLASK_APP=main.py
flask db init
flask db migrate
flask db upgrade
python setup.py
