#!/usr/bin/python3

from bs4 import BeautifulSoup
import requests
import pandas as pd
import asyncio
import tqdm
import sys
import os


def generate_occupation_group_link(insert):
    return "https://www.bls.gov{}".format(insert).lstrip().rstrip()

def parseOccupationLink(link):
    start = len("https://www.bls.gov/oof/")
    return link[start:]

occupations = requests.get("https://www.bls.gov/ooh/")
soup = BeautifulSoup(occupations.text, 'html.parser')
occlist = soup.find(id="ooh-occupation-list")
atags = occlist.find_all('a')

occpages = [generate_occupation_group_link(a.get('href')) for a in atags]

newSoup = None
bls_occ_links = set()

if not os.path.exists("links.txt"):
    for o in tqdm.tqdm(occpages, desc="fetching occ urls"):
         r = requests.get(o)
         newSoup = BeautifulSoup(r.text, 'html.parser')
         occs = newSoup.find(id="landing-page-table")
         if occs:
            occ_as = occs.find_all('a')
            for _ in occ_as:
                bls_occ_links.add(generate_occupation_group_link(_.get('href')))

    with open("links.txt", "w+") as links:
        for l in bls_occ_links:
            links.write(l)
            links.write("\n")
links = open("links.txt")
bls_occ_links = [l for l in links.readlines()]


ids = []
titles = []
descriptions = []
urls = []

for idx, occ in enumerate(tqdm.tqdm(bls_occ_links, desc="grabbing data")):
    try:
        occ = occ.replace("\n", "")
        r = requests.get(occ)
        newSoup = BeautifulSoup(r.text, 'html.parser')
        title = newSoup.find_all('h1')[0]
        title = title.string.replace("<h1>","").replace("</h1>", "").lstrip().rstrip()
        title = title[:-1] if title[-1] == "s" else title
        ps = newSoup.find_all('p')
        desc = ps.pop(0).string.replace("<p>","").replace("</p>", "").lstrip().rstrip()
        if "Please enable javascript" in desc:
            desc = ps.pop(0).string.replace("<p>","").replace("</p>", "").lstrip().rstrip()
        titles.append(title)
        descriptions.append(desc)
        urls.append(occ)
        ids.append(idx + 1)
    except:
        print("Failed: {}".format(sys.exc_info()), idx)

data = pd.DataFrame({"title" : titles, "description" : descriptions
    ,"categoryid" : ids, "bls_url" : urls}
)

if len(data) < 1:
    pass
else:
    data.to_csv("bls_data.csv", index=False, header=True)

